package com.finalproj.missingitnow.member.model.service;

import java.util.List;

import com.finalproj.missingitnow.common.page.PageInfoDTO;
import com.finalproj.missingitnow.member.model.dto.PurchasedListDTO;

public interface PurchaseListService {

	int selectTotalCount(String userNo);

	List<PurchasedListDTO> selectPurchaseList(PageInfoDTO pageInfo, String userNo, int startRow, int endRow);

	
	int selectSearchCount(String userNo, String condition, String value);

	List<PurchasedListDTO> searchPurchaseList(PageInfoDTO pageInfo, String userNo, String condition, String value,
			int startRow, int endRow);

	



}
