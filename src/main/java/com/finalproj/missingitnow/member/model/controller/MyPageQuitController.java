package com.finalproj.missingitnow.member.model.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.finalproj.missingitnow.member.model.dto.PrivateMemberDTO;
import com.finalproj.missingitnow.member.model.service.MemberService;

@Controller
@RequestMapping("member/quit")
public class MyPageQuitController {
	
	private final MemberService memberService;
	
	@Autowired
	public MyPageQuitController(MemberService memberService) {
		super();
		this.memberService = memberService;
	}
	
	
	
	@GetMapping
	public String myPageQuit() {
		
		return "member/quit";
	}
	

	@PostMapping
	public String myPageQuitProcess(@ModelAttribute PrivateMemberDTO member, HttpSession session, RedirectAttributes rttr) {
		
		memberService.userQuit(member);
		
		String loginMessage = "탈퇴가 완료되었습니다. 그동안 이용해주셔서 감사합니다.";
		
		rttr.addFlashAttribute("loginMessage", loginMessage);
		
		session.invalidate();
		
		return "redirect:/";
	}
}
