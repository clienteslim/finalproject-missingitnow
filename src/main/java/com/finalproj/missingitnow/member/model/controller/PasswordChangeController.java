package com.finalproj.missingitnow.member.model.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.finalproj.missingitnow.member.model.dto.PasswordChangeDTO;
import com.finalproj.missingitnow.member.model.dto.PrivateMemberDTO;
import com.finalproj.missingitnow.member.model.service.MemberService;

@Controller
@RequestMapping("member/pwdChange")
public class PasswordChangeController {
	
	private final BCryptPasswordEncoder passwordEncoder;
	private final MemberService memberService;
	
	@Autowired              
	public PasswordChangeController(MemberService memberService, BCryptPasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;
		this.memberService = memberService;
	}
	
	@GetMapping
	public String pwdChange() {
		
		return "member/pwdChange";
	}

	@PostMapping
	public String pwdChangeProcess(@ModelAttribute PasswordChangeDTO pwdChange, PrivateMemberDTO member, BCryptPasswordEncoder passwordEncoder,
			RedirectAttributes rttr) {
		
		
		String currentPassword = pwdChange.getCurrentPassword();
		
		String EncPassword = memberService.selectEncPassword(member);
		
		boolean pwdMatches = passwordEncoder.matches(currentPassword, EncPassword);
		
		
		if(pwdMatches==false) {
			
			
			String message = "입력된 현재 비밀번호와 실제 비밀번호가 다릅니다.";
			
			rttr.addFlashAttribute("message", message);
			
			return "redirect:/member/pwdChange";
			
		}
		
		
		/*화면에서 넘겨받은 비번 입력값의 변수 선언*/
		String checkNewPassword = pwdChange.getNewPassword();
		String checkNewPasswordRetype = pwdChange.getNewPasswordRetype();
		
		String realNewPassword ="";
		
		
		/*로그인한 회원 Id를 세션에서 추출해 변수로 받는다.*/
		
		member.setUserId(pwdChange.getUserId());
		
		boolean pwdMatches2 = passwordEncoder.matches(checkNewPassword, EncPassword);
		boolean pwdMatches3 = passwordEncoder.matches(checkNewPasswordRetype, EncPassword);
		
		
		if(pwdMatches2 == true || pwdMatches3 == true ) {
			
			String message = "현재 사용중인 비밀번호로 변경하실 수 없습니다.";
			
			rttr.addFlashAttribute("message", message);
			
			
			return "redirect:/member/pwdChange";
			
		}
		
		
		if(checkNewPassword.equals(checkNewPasswordRetype)) {
			

			
			realNewPassword = checkNewPassword;

			member.setUserPwd(passwordEncoder.encode(realNewPassword));
			
			
			memberService.pwdChange(member);
			
			String message = "비밀번호가 변경되었습니다.";
			
			rttr.addFlashAttribute("message", message);
			
			
			
		} else if(!(checkNewPassword.equals(checkNewPasswordRetype))) {
			
			String message = "비밀번호가 일치하지 않습니다.";
			
			rttr.addFlashAttribute("message", message);
			
			
			return "redirect:/member/pwdChange";
		}
		
		

		
		return "redirect:/member/myPageMain";
	}
	
	
}
