package com.finalproj.missingitnow.member.model.dto;

public class MyPageModifyDTO {
	
	private String userCurrentAddress;
	private String userAddress;
	private String userCurrentContacts;
	private String userContacts;
	private String userCurrentEmail;
	private String userEmail;
	
	
	
	
	public MyPageModifyDTO() {
		super();
	}




	public MyPageModifyDTO(String userCurrentAddress, String userAddress, String userCurrentContacts,
			String userContacts, String userCurrentEmail, String userEmail) {
		super();
		this.userCurrentAddress = userCurrentAddress;
		this.userAddress = userAddress;
		this.userCurrentContacts = userCurrentContacts;
		this.userContacts = userContacts;
		this.userCurrentEmail = userCurrentEmail;
		this.userEmail = userEmail;
	}




	public String getUserCurrentAddress() {
		return userCurrentAddress;
	}




	public void setUserCurrentAddress(String userCurrentAddress) {
		this.userCurrentAddress = userCurrentAddress;
	}




	public String getUserAddress() {
		return userAddress;
	}




	public void setUserAddress(String userAddress) {
		this.userAddress = userAddress;
	}




	public String getUserCurrentContacts() {
		return userCurrentContacts;
	}




	public void setUserCurrentContacts(String userCurrentContacts) {
		this.userCurrentContacts = userCurrentContacts;
	}




	public String getUserContacts() {
		return userContacts;
	}




	public void setUserContacts(String userContacts) {
		this.userContacts = userContacts;
	}




	public String getUserCurrentEmail() {
		return userCurrentEmail;
	}




	public void setUserCurrentEmail(String userCurrentEmail) {
		this.userCurrentEmail = userCurrentEmail;
	}




	public String getUserEmail() {
		return userEmail;
	}




	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}




	@Override
	public String toString() {
		return "MyPageModifyDTO [userCurrentAddress=" + userCurrentAddress + ", userAddress=" + userAddress
				+ ", userCurrentContacts=" + userCurrentContacts + ", userContacts=" + userContacts
				+ ", userCurrentEmail=" + userCurrentEmail + ", userEmail=" + userEmail + "]";
	}









	
	
	
	
	
}
