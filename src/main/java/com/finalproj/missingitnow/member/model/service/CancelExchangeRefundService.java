package com.finalproj.missingitnow.member.model.service;

import java.util.List;

import com.finalproj.missingitnow.common.page.PageInfoDTO;
import com.finalproj.missingitnow.member.model.dto.CancelExchangeRefundDTO;
import com.finalproj.missingitnow.member.model.dto.PurchasedListDTO;
import com.finalproj.missingitnow.member.model.dto.RefundDTO;

public interface CancelExchangeRefundService {

	
	int selectExchangeTotalCount(String userNo);

	int selectRefundTotalCount(String userNo);
	
	
	int selectExchangeTotalCount(String userNo, String condition, String value);

	int selectRefundTotalCount(String userNo, String condition, String value);


	List<CancelExchangeRefundDTO> selectExchangeList(PageInfoDTO exchangePageInfo, String userNo, int startRow, int endRow);

	List<RefundDTO> selectRefundList(PageInfoDTO refundPageInfo, String userNo, int startRow, int endRow);


	List<CancelExchangeRefundDTO> selectExchangeList(PageInfoDTO exchangePageInfo, String userNo, String exchangeCondition,
			String exchangeValue, int exchangeStartRow, int exchangeEndRow);

	List<RefundDTO> selectRefundList(PageInfoDTO refundPageInfo, String userNo, String refundCondition,
			String refundValue, int refundStartRow, int refundEndRow);





}
