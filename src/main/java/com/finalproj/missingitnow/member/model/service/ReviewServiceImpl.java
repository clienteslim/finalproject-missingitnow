package com.finalproj.missingitnow.member.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.finalproj.missingitnow.common.page.PageInfoDTO;
import com.finalproj.missingitnow.member.model.dao.ReviewMapper;
import com.finalproj.missingitnow.member.model.dto.MyPageReviewDTO;
import com.finalproj.missingitnow.member.model.dto.PurchasedListDTO;

@Service
public class ReviewServiceImpl implements ReviewService{

	ReviewMapper mapper;
	
	@Autowired
	public ReviewServiceImpl(ReviewMapper mapper) {
		super();
		this.mapper = mapper;
	}

	@Override
	public int selectTotalCount(String userNo) {
		
		return mapper.selectTotalCount(userNo);
	}

	@Override
	public List<MyPageReviewDTO> selectReviewList(PageInfoDTO pageInfo, String userNo, int startRow, int endRow) {
		
		return mapper.selectReviewList(pageInfo, userNo, startRow, endRow);
	}

	@Override
	public int selectSearchCount(String userNo, String condition, String value) {

		return mapper.selectTotalCount(userNo, condition, value);
	}

	@Override
	public List<PurchasedListDTO> selectReviewList2(PageInfoDTO pageInfo, String userNo, String condition, String value,
			int startRow, int endRow) {
		
		return mapper.selectReviewList2(pageInfo, userNo, condition, value, startRow, endRow);
	}

}
