package com.finalproj.missingitnow.member.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.finalproj.missingitnow.common.page.PageInfoDTO;
import com.finalproj.missingitnow.member.model.dao.CancelExchangeRefundMapper;
import com.finalproj.missingitnow.member.model.dto.CancelExchangeRefundDTO;
import com.finalproj.missingitnow.member.model.dto.PurchasedListDTO;
import com.finalproj.missingitnow.member.model.dto.RefundDTO;

@Service
public class CancelExchangeRefundServiceImpl implements CancelExchangeRefundService {
	
	private final CancelExchangeRefundMapper mapper;
	
	
	@Autowired
	public CancelExchangeRefundServiceImpl(CancelExchangeRefundMapper mapper) {
		super();
		this.mapper = mapper;
	}

	@Override
	public int selectExchangeTotalCount(String userNo) {
		
		return mapper.selectExchangeTotalCount(userNo);
	}
	
	@Override
	public int selectRefundTotalCount(String userNo) {
		
		return mapper.selectRefundTotalCount(userNo);
	}

	@Override
	public int selectExchangeTotalCount(String userNo, String condition, String value) {
		
		return mapper.selectExchangeTotalCount(userNo, condition, value);
	}

	@Override
	public int selectRefundTotalCount(String userNo, String condition, String value) {
		
		return mapper.selectRefundTotalCount(userNo, condition, value);
	}
	
	@Override
	public List<CancelExchangeRefundDTO> selectExchangeList(PageInfoDTO exchangePageInfo, String userNo, int exchangeStartRow,
			int exchangeEndRow) {
		
		return mapper.selectExchangeList(exchangePageInfo, userNo,
				exchangeStartRow, exchangeEndRow);
	}
	
	
	@Override
	public List<RefundDTO> selectRefundList(PageInfoDTO refundPageInfo, String userNo, int refundStartRow,
			int refundEndRow) {
		
		return mapper.selectRefundList(refundPageInfo, userNo,
				refundStartRow, refundEndRow);
	}
	
	@Override
	public List<CancelExchangeRefundDTO> selectExchangeList(PageInfoDTO exchangePageInfo, String userNo,
			String exchangeCondition, String exchangeValue, int exchangeStartRow, int exchangeEndRow) {
		
		return mapper.searchExchangeList(exchangePageInfo, 
				userNo, exchangeCondition, exchangeValue,
				exchangeStartRow, exchangeEndRow);
	}


	@Override
	public List<RefundDTO> selectRefundList(PageInfoDTO refundPageInfo, String userNo, String refundCondition,
			String refundValue, int refundStartRow, int refundEndRow) {
	
		return mapper.searchRefundList(refundPageInfo, 
				userNo, refundCondition, refundValue,
				refundStartRow, refundEndRow);
	}






	
	
}
