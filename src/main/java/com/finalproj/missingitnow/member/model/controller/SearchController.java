package com.finalproj.missingitnow.member.model.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.finalproj.missingitnow.common.page.PageInfoDTO;
import com.finalproj.missingitnow.common.page.Pagenation;
import com.finalproj.missingitnow.common.search.member.Search;
import com.finalproj.missingitnow.common.search.member.SearchForExchangeRefund;
import com.finalproj.missingitnow.member.model.dto.CancelExchangeRefundDTO;
import com.finalproj.missingitnow.member.model.dto.PrivateMemberDTO;
import com.finalproj.missingitnow.member.model.dto.PurchasedListDTO;
import com.finalproj.missingitnow.member.model.dto.RefundDTO;
import com.finalproj.missingitnow.member.model.service.CancelExchangeRefundService;
import com.finalproj.missingitnow.member.model.service.PurchaseListService;
import com.finalproj.missingitnow.member.model.service.QnaBoardService;
import com.finalproj.missingitnow.member.model.service.ReviewService;


@Controller
@RequestMapping("/member/search/*")
public class SearchController {
	
private final PurchaseListService purchaseService;
private final ReviewService reviewService;
private final QnaBoardService qnaService;
private final CancelExchangeRefundService cancelExchangeRefundService;
	
	@Autowired
	public SearchController(PurchaseListService purchaseService
			, ReviewService reviewService, QnaBoardService qnaService 
			, CancelExchangeRefundService cancelExchangeRefundService) {
		
		super();
		this.purchaseService = purchaseService;
		this.reviewService = reviewService;
		this.qnaService = qnaService;
		this.cancelExchangeRefundService = cancelExchangeRefundService;
	}
	
		
	@RequestMapping(value = "/member/search/purchaseList", method = { RequestMethod.GET})
	public String searchPurchaseList(@RequestParam(value="currentPage", required=false, defaultValue="1") String currentPage
			, @RequestParam(value="userNo", required=false) String userNo
			, @ModelAttribute Search search
			, PrivateMemberDTO member
			, Model model) {
		
		String condition = search.getCondition();
		
		String value = search.getValue();
		
	
		int pageNo = 1;
		
		

		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.valueOf(currentPage);
			

			if(pageNo <= 0) {
				pageNo = 1;
			}
		}
		

		int totalCount = purchaseService.selectSearchCount(userNo, condition, value);

		int limit = 10;

		int buttonAmount = 5;
		

		PageInfoDTO pageInfo = Pagenation.getPageInfo(pageNo, totalCount, limit, buttonAmount);
		
		System.out.println("pageInfo : " + pageInfo);
		
		int startRow = pageInfo.getStartRow();
		
		int endRow = pageInfo.getEndRow();
		
		
		List<PurchasedListDTO> purchaseList = purchaseService.searchPurchaseList(pageInfo, userNo, condition, value, startRow, endRow);
		
		model.addAttribute("purchaseList", purchaseList);
		model.addAttribute("pageInfo", pageInfo);
		model.addAttribute("condition", condition);
		model.addAttribute("searchValue", value);
		
		return "member/purchaseList";
	}
	
	
	@RequestMapping(value = "/member/search/cancelExchangeRefundList", method = { RequestMethod.GET})
	public String searchCancelExcangeRefundList(@RequestParam(value="currentPage", required=false, defaultValue="1") String currentPage
			, @RequestParam(value="userNo", required=false) String userNo
			, @ModelAttribute SearchForExchangeRefund search
			, PrivateMemberDTO member
			, Model model) {
		
		String exchangeCondition = search.getExchangeCondition();
		String exchangeValue = search.getExchangeValue();
		
		String refundCondition = search.getRefundCondition();
		String refundValue = search.getRefundValue();
		
		
		int pageNo = 1;
		
		
		
		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.valueOf(currentPage);
			
			
			if(pageNo <= 0) {
				pageNo = 1;
			}
		}
		
		
		int exchangetotalCount = cancelExchangeRefundService.selectExchangeTotalCount(userNo, exchangeCondition, exchangeValue);
		int refundTotalCount = cancelExchangeRefundService.selectRefundTotalCount(userNo, refundCondition, refundValue);
		
		
		int limit = 10;
		
		int buttonAmount = 5;
		
		
		PageInfoDTO exchangePageInfo = Pagenation.getPageInfo(pageNo, exchangetotalCount, limit, buttonAmount);
		PageInfoDTO refundPageInfo = Pagenation.getPageInfo(pageNo, refundTotalCount, limit, buttonAmount);
		
		System.out.println("exchangePageInfo : " + exchangePageInfo);
		System.out.println("refundPageInfo : " + refundPageInfo);

		
		int exchangeStartRow = exchangePageInfo.getStartRow();
		int exchangeEndRow = exchangePageInfo.getEndRow();
		
		int refundStartRow = refundPageInfo.getStartRow();
		int refundEndRow = refundPageInfo.getEndRow();
		
		List<CancelExchangeRefundDTO> exchangeList = cancelExchangeRefundService.selectExchangeList(exchangePageInfo, userNo, exchangeCondition, exchangeValue, exchangeStartRow, exchangeEndRow);
		List<RefundDTO> refundList = cancelExchangeRefundService.selectRefundList(refundPageInfo, userNo, refundCondition, refundValue, refundStartRow, refundEndRow);
		
		model.addAttribute("exchangeList", exchangeList);
		model.addAttribute("refundList", refundList);
		
		model.addAttribute("exchangePageInfo", exchangePageInfo);
		model.addAttribute("refundPageInfo", refundPageInfo);
		
		model.addAttribute("exchangeCondition", exchangeCondition);
		model.addAttribute("refundCondition", refundCondition);

		model.addAttribute("exchangeValue", exchangeValue);
		model.addAttribute("refundValue", refundValue);
		
		return "member/cancelExchangeRefund";
	}
	
	
	
	
	/*refund ajax*/
	
	@RequestMapping(value = "/member/search/refundList", method = { RequestMethod.GET})
	@ResponseBody
	public List<RefundDTO> searchRefundList(@RequestParam(value="currentPage", required=false, defaultValue="1") String currentPage
			, @RequestParam(value="arr[]") List<String> arr
	        
			, PrivateMemberDTO member
			, Model model) {
		
		
		String refundCondition = arr.get(0);
		String refundValue = arr.get(1);
		String userNo = arr.get(2);
		
		System.out.println(refundCondition);
		System.out.println(refundValue);
		
		
		System.out.println(userNo);
		
		
		
		int pageNo = 1;
		
		
		
		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.valueOf(currentPage);
			
			
			if(pageNo <= 0) {
				pageNo = 1;
			}
		}
		
		
		int refundTotalCount = cancelExchangeRefundService.selectRefundTotalCount(userNo, refundCondition, refundValue);
		
		
		int limit = 8;
		
		int buttonAmount = 5;
		
		
		PageInfoDTO refundPageInfo = Pagenation.getPageInfo(pageNo, refundTotalCount, limit, buttonAmount);
		
		System.out.println("refundPageInfo : " + refundPageInfo);

		
		
		int refundStartRow = refundPageInfo.getStartRow();
		int refundEndRow = refundPageInfo.getEndRow();
		
		List<RefundDTO> refundList = cancelExchangeRefundService.selectRefundList(refundPageInfo, userNo, refundCondition, refundValue, refundStartRow, refundEndRow);
		
		System.out.println("test");
		
		
		model.addAttribute("refundList", refundList);
		
		model.addAttribute("refundPageInfo", refundPageInfo);
		
		model.addAttribute("refundCondition", refundCondition);

		model.addAttribute("refundValue", refundValue);
		
		return refundList;
	}
	

	
		
	@RequestMapping(value = "/member/search/QNAList", method = { RequestMethod.GET})
	public String searchQNAList(@RequestParam(value="currentPage", required=false, defaultValue="1") String currentPage
			, @RequestParam(value="userNo", required=false) String userNo
			, @ModelAttribute Search search
			, PrivateMemberDTO member
			, Model model) {
		
		String condition = search.getCondition();
		
		String value = search.getValue();
		
		
		SimpleDateFormat format1 = new SimpleDateFormat ("yy");
		Date time = new Date();
		String time1 = format1.format(time);
		
		if(condition.equals("qnaDate")) {
			
			StringBuffer sb = new StringBuffer();
			
			
			
			//case1 .을 입력한 경우 /로 바꿔준다 = "21.03.11";
			value = value.replace(".", "/");
			
			
			//case2 -로 입력한 경우 /로 바꿔준다 = "21-03-11";
			value = value.replace("-", "/");
			System.out.println("value : " + value);
			
			
						
			int dateLength = value.length();
			
			int firstSlash = value.indexOf("/");

			int lastSlash = value.lastIndexOf("/");
			
			System.out.println("firstSlash : " + firstSlash);
			System.out.println("lastSlash : " + lastSlash);
			
			//case3 월단위에 0을 뺀 경우 = "21/3/11";
				
			if(dateLength==7 && firstSlash==2 && lastSlash==4) {
				
				sb.append(value);
				sb.insert(3, "0");
				String sb2 = sb.toString();
				
				value = sb2;
			}
			
			//case4 일단위에 0을 뺀 경우 = "21/03/2";
			
			if(dateLength==7 && lastSlash==5) {
				
				sb.append(value);
				sb.insert(6, "0");
				String sb2 = sb.toString();
				
				value = sb2;
				
			}
			
			
			//case5 월과 일단위에 날짜를 뺀 경우 = "21/3/2"
			
			if(dateLength==6 && firstSlash==2) {
				
				sb.append(value);
				sb.insert(3, "0");
				sb.insert(6, "0");
				String sb2 = sb.toString();
				
				value = sb2;
								
			}
			
			
			
			
			//case6 월/일만 입력한 경우 = "3/11";
			
			else if(dateLength==4 && firstSlash==1) {
				
				sb.append(value);
				sb.insert(0, time1+"/0");
				String sb2 = sb.toString();
				
				value = sb2;
				
			}
			
			//case7 년월일 구분자 없이 입력한 경우 = "21311";
			else if(dateLength==5 && !(firstSlash==2)) {
				
				sb.append(value);
				sb.insert(2, "/");
				sb.insert(3, "0");
				sb.insert(5, "/");
				
				String sb2 = sb.toString();
				
				value = sb2;
			}
			
			
			//case8 년월일 구분자 모두 입력했으나 앞에 20을 넣은 경우 = "2021/03/11";

			else if(dateLength==10) {
				
				value = value.substring(2,10);
				
			}
				
						
			//case9 년월일 입력했으나 구분자없고 앞에 20넣은 경우  = "20210311";
			else if(dateLength==8 && !(firstSlash==2)) {
				
				value = value.substring(2,8);
				sb.append(value); 
				sb.insert(2, "/" );
				sb.insert(5, "/");				
				String sb2 = sb.toString();
				
				value = sb2;
			}
			
			//case10 년월일 구분자없이 입력한 경우 = "210311";
			else if (dateLength==6 && !(firstSlash==2)) {
				
				sb.append(value);
				sb.insert(2, "/" );
				sb.insert(5, "/");				
				String sb2 = sb.toString();
				
				value = sb2;
							
			//case11 구분자 없이 월일만 입력한 경우 = "0311";
			} else if (dateLength==4 && !(firstSlash==1)) {
								
				sb.append(value); 
				sb.insert(2, "/" );
				String sb2 = sb.toString();
				
				value = time1 + "/" + sb2;				
				
			//case12 월일만 구분자 입력하여 입력한 경우 = "03/11";
			} else if (firstSlash==2 && dateLength==5) {
				
				value = time1 + "/" + value;
				
			}
			
		}
		

		
		
		int pageNo = 1;
		
		
		
		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.valueOf(currentPage);
			
			
			if(pageNo <= 0) {
				pageNo = 1;
			}
		}
		
		
		int totalCount = qnaService.selectSearchCount(userNo, condition, value);
		
		int limit = 10;
		
		int buttonAmount = 5;
		
		
		PageInfoDTO pageInfo = Pagenation.getPageInfo(pageNo, totalCount, limit, buttonAmount);
		
		System.out.println("pageInfo : " + pageInfo);
		
		int startRow = pageInfo.getStartRow();
		
		int endRow = pageInfo.getEndRow();
		
		
		List<PurchasedListDTO> boardList = qnaService.searchQNAList(pageInfo, userNo, condition, value, startRow, endRow);
		
		model.addAttribute("boardList", boardList);
		model.addAttribute("pageInfo", pageInfo);
		model.addAttribute("condition", condition);
		model.addAttribute("searchValue", value);
		
		return "member/QNA";
	}
	
	
	@RequestMapping(value = "/member/search/review", method = { RequestMethod.GET})
	public String searchReviewList(@RequestParam(value="currentPage", required=false, defaultValue="1") String currentPage
			, @RequestParam(value="userNo", required=false) String userNo
			, @ModelAttribute Search search
			, PrivateMemberDTO member
			, Model model) {
		
		String condition = search.getCondition();
		
		String value = search.getValue();
		
		SimpleDateFormat format1 = new SimpleDateFormat ("yy");
		Date time = new Date();
		String time1 = format1.format(time);
		
		if(condition.equals("orderDate")) {
			
			StringBuffer sb = new StringBuffer();
			
			
			
			//case1 .을 입력한 경우 /로 바꿔준다 = "21.03.11";
			value = value.replace(".", "/");
			
			
			//case2 -로 입력한 경우 /로 바꿔준다 = "21-03-11";
			value = value.replace("-", "/");
			System.out.println("value : " + value);
			
			
						
			int dateLength = value.length();
			
			int firstSlash = value.indexOf("/");

			int lastSlash = value.lastIndexOf("/");
			
			System.out.println("firstSlash : " + firstSlash);
			System.out.println("lastSlash : " + lastSlash);
			
			//case3 월단위에 0을 뺀 경우 = "21/3/11";
				
			if(dateLength==7 && firstSlash==2 && lastSlash==4) {
				
				sb.append(value);
				sb.insert(3, "0");
				String sb2 = sb.toString();
				
				value = sb2;
			}
			
			//case4 일단위에 0을 뺀 경우 = "21/03/2";
			
			if(dateLength==7 && lastSlash==5) {
				
				sb.append(value);
				sb.insert(6, "0");
				String sb2 = sb.toString();
				
				value = sb2;
				
			}
			
			
			//case5 월과 일단위에 날짜를 뺀 경우 = "21/3/2"
			
			if(dateLength==6 && firstSlash==2) {
				
				sb.append(value);
				sb.insert(3, "0");
				sb.insert(6, "0");
				String sb2 = sb.toString();
				
				value = sb2;
								
			}
			
			
			
			
			//case6 월/일만 입력한 경우 = "3/11";
			
			else if(dateLength==4 && firstSlash==1) {
				
				sb.append(value);
				sb.insert(0, time1+"/0");
				String sb2 = sb.toString();
				
				value = sb2;
				
			}
			
			//case7 년월일 구분자 없이 입력한 경우 = "21311";
			else if(dateLength==5 && !(firstSlash==2)) {
				
				sb.append(value);
				sb.insert(2, "/");
				sb.insert(3, "0");
				sb.insert(5, "/");
				
				String sb2 = sb.toString();
				
				value = sb2;
			}
			
			
			//case8 년월일 구분자 모두 입력했으나 앞에 20을 넣은 경우 = "2021/03/11";

			else if(dateLength==10) {
				
				value = value.substring(2,10);
				
			}
				
						
			//case9 년월일 입력했으나 구분자없고 앞에 20넣은 경우  = "20210311";
			else if(dateLength==8 && !(firstSlash==2)) {
				
				value = value.substring(2,8);
				sb.append(value); 
				sb.insert(2, "/" );
				sb.insert(5, "/");				
				String sb2 = sb.toString();
				
				value = sb2;
			}
			
			//case10 년월일 구분자없이 입력한 경우 = "210311";
			else if (dateLength==6 && !(firstSlash==2)) {
				
				sb.append(value);
				sb.insert(2, "/" );
				sb.insert(5, "/");				
				String sb2 = sb.toString();
				
				value = sb2;
							
			//case11 구분자 없이 월일만 입력한 경우 = "0311";
			} else if (dateLength==4 && !(firstSlash==1)) {
								
				sb.append(value); 
				sb.insert(2, "/" );
				String sb2 = sb.toString();
				
				value = time1 + "/" + sb2;				
				
			//case12 월일만 구분자 입력하여 입력한 경우 = "03/11";
			} else if (firstSlash==2 && dateLength==5) {
				
				value = time1 + "/" + value;
				
			}
			
		}
		
		
		int pageNo = 1;
		
		
		
		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.valueOf(currentPage);
			
			
			if(pageNo <= 0) {
				pageNo = 1;
			}
		}
		

		
		int totalCount = reviewService.selectSearchCount(userNo, condition, value);
		
		int limit = 10;
		
		int buttonAmount = 5;
		

		
		
		PageInfoDTO pageInfo = Pagenation.getPageInfo(pageNo, totalCount, limit, buttonAmount);
		
		System.out.println("pageInfo : " + pageInfo);
		
		int startRow = pageInfo.getStartRow();
		
		int endRow = pageInfo.getEndRow();

		
		List<PurchasedListDTO> reviewList = reviewService.selectReviewList2(pageInfo, userNo, condition, value, startRow, endRow);
		
		model.addAttribute("reviewList", reviewList);
		model.addAttribute("pageInfo", pageInfo);
		model.addAttribute("condition", condition);
		model.addAttribute("searchValue", value);
		
		return "member/review";
	}
	
	
	
	
	
	
	
	
	
}
