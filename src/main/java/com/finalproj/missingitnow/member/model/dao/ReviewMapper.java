package com.finalproj.missingitnow.member.model.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.finalproj.missingitnow.common.page.PageInfoDTO;
import com.finalproj.missingitnow.member.model.dto.MyPageReviewDTO;
import com.finalproj.missingitnow.member.model.dto.PurchasedListDTO;

public interface ReviewMapper {

	public int selectTotalCount(String userNo);

	public List<MyPageReviewDTO> selectReviewList(@Param("pageInfo")PageInfoDTO pageInfo 
			, @Param("userNo") String userNo
			, @Param("startRow") int startRow, @Param("endRow") int endRow);

	public int selectTotalCount(@Param("userNo") String userNo, @Param("condition") String condition, @Param("value") String value);

	public List<PurchasedListDTO> selectReviewList2(@Param("pageInfo") PageInfoDTO pageInfo
			, @Param("userNo") String userNo
			, @Param("condition") String condition
			, @Param("value") String value
			, @Param("startRow") int startRow, @Param("endRow") int endRow);

}
