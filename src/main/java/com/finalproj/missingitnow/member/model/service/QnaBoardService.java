package com.finalproj.missingitnow.member.model.service;

import java.util.List;

import com.finalproj.missingitnow.common.page.PageInfoDTO;
import com.finalproj.missingitnow.member.model.dto.PurchasedListDTO;
import com.finalproj.missingitnow.member.model.dto.QnaBoardDTO;

public interface QnaBoardService {

	int selectTotalCount(String userNo);

	List<QnaBoardDTO> selectBoardList(PageInfoDTO pageInfo, String userNo, int startRow, int endRow);

	int selectSearchCount(String userNo, String condition, String value);

	List<PurchasedListDTO> searchQNAList(PageInfoDTO pageInfo, String userNo, String condition, String value,
			int startRow, int endRow);
	

}
