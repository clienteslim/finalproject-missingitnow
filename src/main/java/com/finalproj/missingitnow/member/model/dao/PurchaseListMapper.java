package com.finalproj.missingitnow.member.model.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.finalproj.missingitnow.common.page.PageInfoDTO;
import com.finalproj.missingitnow.member.model.dto.PurchasedListDTO;

public interface PurchaseListMapper {

	public int selectTotalCount(String userNo);

	public List<PurchasedListDTO> selectPurchaseList(@Param("pageInfo")PageInfoDTO pageInfo 
			, @Param("userNo") String userNo
			, @Param("startRow") int startRow, @Param("endRow") int endRow);

	
	public int selectTotalCount(@Param("userNo") String userNo, @Param("condition") String condition, @Param("value") String value);

	
	public List<PurchasedListDTO> selectPurchaseList2(@Param("pageInfo")PageInfoDTO pageInfo
			, @Param("userNo") String userNo, @Param("condition") String condition
			, @Param("value") String value           
			, @Param("startRow") int startRow, @Param("endRow") int endRow);

	

}
