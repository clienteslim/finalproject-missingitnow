package com.finalproj.missingitnow.member.model.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.finalproj.missingitnow.member.model.dto.PrivateMemberDTO;
import com.finalproj.missingitnow.member.model.service.MemberService;

@Controller
@SessionAttributes("loginMember")
@RequestMapping("/login")
public class LoginPageController {
	
	private final MemberService memberService;
	private final BCryptPasswordEncoder passwordEncoder;
	
	@Autowired
	public LoginPageController(MemberService memberService, BCryptPasswordEncoder passwordEncoder) {
		this.memberService = memberService;
		this.passwordEncoder = passwordEncoder;
	}
	
	
	@GetMapping
	public String login() {
		
		return "member/login";
	}
	
	@PostMapping("memlogin")
	public String login(@ModelAttribute PrivateMemberDTO member, Model model
			, RedirectAttributes rttr) {
		
		int result = memberService.loginCheck(member);
		int leaveCheck = memberService.leaveCheck(member);
		
		/*입력받은 Id 값을 매퍼의 매개변수로 하여 DB에서 암호화된 비밀번호를 가져옴*/
		String EncPassword = memberService.selectEncPassword(member.getUserId());
		
		String inputPassword = member.getUserPwd();

		/*입력받은 암호값과 DB상의 암호화된 값을 비교하여 일치여부를 확인함 */
		boolean pwdMatches = passwordEncoder.matches(inputPassword, EncPassword);
		
		
		
		if(leaveCheck==0) {
			/*탈퇴한 회원을 조회해서 0이라면 로그인 가능하므로 진행함*/
			
			if(result==1 && (pwdMatches==true)) {
				
				/* id가 존재하면서 비밀번호가 일치하면 로그인을 진행함 */
				
				model.addAttribute("loginMember", memberService.loginMember(member));
				
				
			} else if(result==0) {
				
				/* 조회시 id가 존재하지 않는 경우 */
				
				String loginMessage = "로그인 실패 : 존재하지 않는 아이디입니다.";
				
				rttr.addFlashAttribute("loginMessage", loginMessage);
				
				return "redirect:/";
				
				
			} else if(pwdMatches==false) {
				
				/* 비밀번호가 일치하지 않는 경우 */
				
				String loginMessage = "로그인 실패 : 비밀번호가 다릅니다.";
				
				rttr.addFlashAttribute("loginMessage", loginMessage);
				
				return "redirect:/";
				
				
			}
		
		} else {
			
			/*leaveCheck가 0이 아니면 1이므로, 탈퇴한 회원의 경우*/
			
			String loginMessage = "로그인 실패 : 탈퇴한 회원입니다.";
			
			rttr.addFlashAttribute("loginMessage", loginMessage);
			
			return "redirect:/";
			
		}
		
				
		return "redirect:/";
	}
	
	
}
