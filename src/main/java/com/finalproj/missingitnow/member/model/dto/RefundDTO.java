package com.finalproj.missingitnow.member.model.dto;

import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonFormat;


public class RefundDTO {
	
	
	private String returnNo;
	private String deliveryNo;
	private String userNo;
	private String orderNo;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
	private java.sql.Date returnDate;
	
	private String returnChk;
	private String refundChk;
	private String userName;
	private String prodName;
	private String orderPrice;
	
	
	public RefundDTO() {
		super();
	}


	public RefundDTO(String returnNo, String deliveryNo, String userNo, String orderNo, Date returnDate,
			String returnChk, String refundChk, String userName, String prodName, String orderPrice) {
		super();
		this.returnNo = returnNo;
		this.deliveryNo = deliveryNo;
		this.userNo = userNo;
		this.orderNo = orderNo;
		this.returnDate = returnDate;
		this.returnChk = returnChk;
		this.refundChk = refundChk;
		this.userName = userName;
		this.prodName = prodName;
		this.orderPrice = orderPrice;
	}


	public String getReturnNo() {
		return returnNo;
	}


	public void setReturnNo(String returnNo) {
		this.returnNo = returnNo;
	}


	public String getDeliveryNo() {
		return deliveryNo;
	}


	public void setDeliveryNo(String deliveryNo) {
		this.deliveryNo = deliveryNo;
	}


	public String getUserNo() {
		return userNo;
	}


	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}


	public String getOrderNo() {
		return orderNo;
	}


	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}


	public java.sql.Date getReturnDate() {
		return returnDate;
	}


	public void setReturnDate(java.sql.Date returnDate) {
		this.returnDate = returnDate;
	}


	public String getReturnChk() {
		return returnChk;
	}


	public void setReturnChk(String returnChk) {
		this.returnChk = returnChk;
	}


	public String getRefundChk() {
		return refundChk;
	}


	public void setRefundChk(String refundChk) {
		this.refundChk = refundChk;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getProdName() {
		return prodName;
	}


	public void setProdName(String prodName) {
		this.prodName = prodName;
	}


	public String getOrderPrice() {
		return orderPrice;
	}


	public void setOrderPrice(String orderPrice) {
		this.orderPrice = orderPrice;
	}


	@Override
	public String toString() {
		return "RefundDTO [returnNo=" + returnNo + ", deliveryNo=" + deliveryNo + ", userNo=" + userNo + ", orderNo="
				+ orderNo + ", returnDate=" + returnDate + ", returnChk=" + returnChk + ", refundChk=" + refundChk
				+ ", userName=" + userName + ", prodName=" + prodName + ", orderPrice=" + orderPrice + "]";
	}





	
	
}
