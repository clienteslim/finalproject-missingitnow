package com.finalproj.missingitnow.member.model.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.finalproj.missingitnow.common.page.PageInfoDTO;
import com.finalproj.missingitnow.member.model.dto.CancelExchangeRefundDTO;
import com.finalproj.missingitnow.member.model.dto.PurchasedListDTO;
import com.finalproj.missingitnow.member.model.dto.RefundDTO;

public interface CancelExchangeRefundMapper {

	int selectExchangeTotalCount(String userNo);
	
	int selectRefundTotalCount(String userNo);
	
	
	int selectExchangeTotalCount(@Param("userNo") String userNo
			, @Param("exchangeCondition") String exchangeCondition
			, @Param("exchangeValue") String exchangeValue);
	
	int selectRefundTotalCount(@Param("userNo") String userNo
			, @Param("refundCondition") String refundCondition
			, @Param("refundValue") String refundValue);
	
	
	List<CancelExchangeRefundDTO> selectExchangeList(@Param("exchangePageInfo")PageInfoDTO exchangePageInfo 
			, @Param("userNo") String userNo
			, @Param("exchangeStartRow") int exchangeStartRow
			, @Param("exchangeEndRow") int exchangeEndRow);

	List<RefundDTO> selectRefundList(@Param("refundPageInfo")PageInfoDTO refundPageInfo
			, @Param("userNo")String userNo
			, @Param("refundStartRow") int refundStartRow
			, @Param("refundEndRow") int refundEndRow);
	
	
	
	List<CancelExchangeRefundDTO> searchExchangeList(@Param("exchangePageInfo")PageInfoDTO exchangePageInfo
			, @Param("userNo")String userNo
			, @Param("exchangeCondition")String exchangeCondition
			, @Param("exchangeValue") String exchangeValue
			, @Param("exchangeStartRow") int exchangeStartRow 
			, @Param("exchangeEndRow") int exchangeEndRow);

	List<RefundDTO> searchRefundList(@Param("refundPageInfo")PageInfoDTO refundPageInfo
			, @Param("userNo") String userNo
			, @Param("refundCondition") String refundCondition
			, @Param("refundValue") String refundValue
			, @Param("refundStartRow") int refundStartRow
			, @Param("refundEndRow") int refundEndRow);



	

	



}
