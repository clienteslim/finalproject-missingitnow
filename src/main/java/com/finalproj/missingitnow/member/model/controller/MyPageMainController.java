package com.finalproj.missingitnow.member.model.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.finalproj.missingitnow.member.model.dto.MyPageModifyDTO;
import com.finalproj.missingitnow.member.model.dto.PrivateMemberDTO;
import com.finalproj.missingitnow.member.model.service.MemberService;

@Controller
@RequestMapping("member/myPageMain")
public class MyPageMainController {
	
	private final MemberService memberService;
	
	@Autowired
	public MyPageMainController(MemberService memberService) {
		this.memberService = memberService;
	}
	
	@GetMapping
	public String myPageMain() {
		
		return "member/myPageMain";
	}
	
	
	@PostMapping
	public String myPageModify(@ModelAttribute MyPageModifyDTO modify, PrivateMemberDTO member, HttpSession session, RedirectAttributes rttr) {

		
		String userCurrentAddress = modify.getUserCurrentAddress();
		
		String modifyAddress = modify.getUserAddress();
		
		String userCurrentContacts = modify.getUserCurrentContacts();
		
		String modifyContacts = modify.getUserContacts();
		
		String userCurrentEmail = modify.getUserCurrentEmail();
		
		String modifyEmail = modify.getUserEmail();
				
		
		System.out.println("userCurrentAddress : " + userCurrentAddress);
		System.out.println("modifyAddress : " + modifyAddress);
		
		System.out.println("userCurrentContacts : " + userCurrentContacts);
		System.out.println("modifyContacts : " + modifyContacts);
		
		System.out.println("userCurrentEmail : " + userCurrentEmail);
		System.out.println("modifyEmail : " + modifyEmail);
		
		
		if (!(userCurrentAddress.equals(modifyAddress)) || !(userCurrentContacts.equals(modifyContacts)) || !(userCurrentEmail.equals(modifyEmail)) ) {
			
			String modifyMessage = "정보가 변경되었습니다.";
			System.out.println(modifyMessage);
			
			rttr.addFlashAttribute("modifyMessage", modifyMessage);
						
		} else if ((userCurrentAddress.equals(modifyAddress)) && (userCurrentContacts.equals(modifyContacts)) && (userCurrentEmail.equals(modifyEmail)) ) {
			
			String modifyMessage = "변경된 정보가 없습니다.";
			System.out.println(modifyMessage);
			rttr.addFlashAttribute("modifyMessage", modifyMessage);
		}
		
		
		memberService.userUpdate(member);

		
		session.invalidate();

		
		return "redirect:/corporation/loginPage";	
		
	}
	
}
