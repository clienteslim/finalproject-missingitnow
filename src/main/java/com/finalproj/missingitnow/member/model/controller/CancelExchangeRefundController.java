package com.finalproj.missingitnow.member.model.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.finalproj.missingitnow.common.page.PageInfoDTO;
import com.finalproj.missingitnow.common.page.Pagenation;
import com.finalproj.missingitnow.member.model.dto.CancelExchangeRefundDTO;
import com.finalproj.missingitnow.member.model.dto.PrivateMemberDTO;
import com.finalproj.missingitnow.member.model.dto.RefundDTO;
import com.finalproj.missingitnow.member.model.service.CancelExchangeRefundService;

@Controller
@RequestMapping("/member/cancelExchangeRefund")
public class CancelExchangeRefundController {
	
	private final CancelExchangeRefundService cancelExchangeRefundService;
	
	@Autowired
	public CancelExchangeRefundController(CancelExchangeRefundService cancelExchangeRefundService) {
		super();
		this.cancelExchangeRefundService = cancelExchangeRefundService;
	}
	
	
	@RequestMapping(method = { RequestMethod.POST,RequestMethod.GET })
	public String CancelExchangeRefund(@RequestParam(value="currentPage", required=false) String currentPage
			, @RequestParam(value="userNo", required=false) String userNo
			, PrivateMemberDTO member
			, Model model) {
		
		
		int pageNo = 1;
		
		

		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.valueOf(currentPage);
			

			if(pageNo <= 0) {
				pageNo = 1;
			}
		}
		

		int exchangeTotalCount = cancelExchangeRefundService.selectExchangeTotalCount(userNo);
		int refundTotalCount = cancelExchangeRefundService.selectRefundTotalCount(userNo);

		int limit = 8;

		int buttonAmount = 5;
		

		PageInfoDTO exchangePageInfo = Pagenation.getPageInfo(pageNo, exchangeTotalCount, limit, buttonAmount);
		PageInfoDTO refundPageInfo = Pagenation.getPageInfo(pageNo, refundTotalCount, limit, buttonAmount);
		
		System.out.println("pageInfo : " + exchangePageInfo);
		
				
		int exchangeStartRow = exchangePageInfo.getStartRow();		
		int exchangeEndRow = exchangePageInfo.getEndRow();
		
		
		int refundStartRow = refundPageInfo.getStartRow();
		int refundEndRow = refundPageInfo.getEndRow();
		
		
		List<CancelExchangeRefundDTO> exchangeList = cancelExchangeRefundService.selectExchangeList(exchangePageInfo, userNo, exchangeStartRow, exchangeEndRow);
		
		List<RefundDTO> refundList = cancelExchangeRefundService.selectRefundList(refundPageInfo, userNo, refundStartRow, refundEndRow);
		
		model.addAttribute("exchangeList", exchangeList);
		model.addAttribute("refundList", refundList);
		model.addAttribute("exchangePageInfo", exchangePageInfo);
		model.addAttribute("refundPageInfo", refundPageInfo);
		
		
		return "member/cancelExchangeRefund";
	} 
	
	
}
