package com.finalproj.missingitnow.common.search.member;

public class SearchForExchangeRefund {
	
	private String exchangeCondition;
	private String exchangeValue;
	
	private String refundCondition;
	private String refundValue;
	
	
	
	public SearchForExchangeRefund() {
		super();
	}



	public SearchForExchangeRefund(String exchangeCondition, String exchangeValue, String refundCondition,
			String refundValue) {
		super();
		this.exchangeCondition = exchangeCondition;
		this.exchangeValue = exchangeValue;
		this.refundCondition = refundCondition;
		this.refundValue = refundValue;
	}



	public String getExchangeCondition() {
		return exchangeCondition;
	}



	public void setExchangeCondition(String exchangeCondition) {
		this.exchangeCondition = exchangeCondition;
	}



	public String getExchangeValue() {
		return exchangeValue;
	}



	public void setExchangeValue(String exchangeValue) {
		this.exchangeValue = exchangeValue;
	}



	public String getRefundCondition() {
		return refundCondition;
	}



	public void setRefundCondition(String refundCondition) {
		this.refundCondition = refundCondition;
	}



	public String getRefundValue() {
		return refundValue;
	}



	public void setRefundValue(String refundValue) {
		this.refundValue = refundValue;
	}



	@Override
	public String toString() {
		return "SearchForExchangeRefund [exchangeCondition=" + exchangeCondition + ", exchangeValue=" + exchangeValue
				+ ", refundCondition=" + refundCondition + ", refundValue=" + refundValue + "]";
	}
	
	
	
	
	
	
}
