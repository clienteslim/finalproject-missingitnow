package com.finalproj.missingitnow.common.search.member;

import com.finalproj.missingitnow.common.page.Pagenation;

public class Search extends Pagenation{
	
	private String condition;
	
	private String value;

	public Search() {
		super();
	}

	public Search(String condition, String value) {
		super();
		this.condition = condition;
		this.value = value;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Search [condition=" + condition + ", value=" + value + "]";
	}

	
	
	
	
}
