<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="kr">
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="/missingitnow/resources/js/member/myPageNav.js"></script>
<link rel="stylesheet" href="/missingitnow/resources/css/member/purchaseList.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">


<style>
	
</style>


</head>


<body>
	
 	<div class="header">
	<jsp:include page="../common/header.jsp"/>
	
	</div>  
<div>
<img class="leftBg" src="/missingitnow/resources/images/member/purchaseListLeft.png">
</div> 

<div id="contentForm">
	
    <div id="registFormTitle">
   
   
   		<div></div>
	   <table class="titleTable">
	   
	   		
		   	<tr>
		   		<td class="titleColorBox"><br><br><br><br><c:out value="${loginMember.userName }" />&nbsp;회원님의&nbsp; </td> 
		   		<td class="titleNonColor">구매내역입니다.</td>
		   	</tr>
	   </table>           
   
     	
   </div>

    <div id="totalForm">
        
             	<form name="Paging">
                     <input type="hidden" name="userNo" value="${loginMember.userNo}">				
				</form>
    	
    
    		<div class="leftDiv">
    		
    		
                <table class="myPageNavTable">
                    <tr>
                    <td align="center"><br>  <i id="userImg" class="fas fa-user"></i> <!--프로필 사진 영역-->
                    <br></td>
                    </tr>
                    
                    <tr>
                        <td class="welcomeTd" align="center">
                            <c:out value="${ loginMember.userName }"/>님 안녕하세요!
                        </td>
                    </tr>
					<tr> <td><br></td> </tr>
                    <tr>
                        <td>
                        <a href="${pageContext.servletContext.contextPath}/member/myPageMain">
                        		&nbsp;프로필
                        		</a>
                        		</td>
                    </tr>
                    <tr> <td><br></td> </tr>
                    <tr>
                        <td class="pointTd">
                        <a href="javascript:goPurchaseListPage('${loginMember.userNo}');">
                        &nbsp;구매내역
                        </a>
                        </td>
                    </tr>
                    <tr> <td><br></td> </tr>
                    <tr>
						 <td>
						<a href="javascript:goCancelExchangeRefundPage('${loginMember.userNo}');">
                        &nbsp;취소/교환/반품내역
                        </a>
                        </td>
                    </tr>
                    <tr> <td><br></td> </tr>

                    <tr>
                        <td>
                        <a href="javascript:goQnaPage('${loginMember.userNo}');">
                        &nbsp;Q&A
                        </a>
                        </td>
                    </tr>

                    <tr> <td><br></td> </tr>
<!--                     <tr>
                        <td>&nbsp;쿠폰</td>
                    </tr>
                    <tr> <td><br></td> </tr> -->
                    <tr>
                        <td>
                        <a href="javascript:goReviewPage('${loginMember.userNo}');">
                        &nbsp;리뷰내역
                        </a>
                        </td>
                    </tr>
                    <tr> <td><br></td> </tr>
                </table>

            
        </div>
        
        <div class="centerBlank"><br></div>
        
        
        <div id="colorBox">
    
        </div>
		
		
		
		
        <div id="rightDiv">

            <section>
                    
                    
                    
                    <c:if test="${!empty purchaseList }">
                    	
                <table class="purchaseListTable" border="1">
					<tr><td><br></td></tr>
                    <tr class="bottomLineTr">                        
                        <th>구매일시</th>
                        <th>구매번호</th>
                        <th>주문자</th>
                        <th>주문상품명</th>
                        <th>주문금액</th>
                    </tr>
                    <c:forEach items="${purchaseList}" var="purchase">
                    	<tr class="bottomLineTr" align="center">                    	
                    	<td><c:out value="${purchase.purchasedDate}"/></td>
                    	<td><c:out value="${purchase.purchaseNo}"/></td>            							
                    	<td><c:out value="${purchase.privateMemberDTO.userName}"/></td>
                    	<td><c:out value="${purchase.productDTO.prodName}"/></td>
                    	<td><c:out value="${purchase.orderDTO.orderPrice}"/></td>
                    	<td>
                    	
                    	</td>
                    	</tr>        	
                    	
                    	
                    </c:forEach>                    
			     </table>
	                 
                    
               </c:if>
                    
               <c:if test="${empty purchaseList}"> 
               		
               		<table class="emptyTable">
			         	<tr><td><br><br></td></tr>
			         	<tr>
			         		<td class="emptyMessage" align="center">&nbsp;아직 구매하신 내역이 없습니다.</td>
			         	</tr>
			         	
			         	<tr><td><br><br><br></td></tr>
			         	
			         	<tr>
			         		<td>
			        			<img class="emptyBox" src="/missingitnow/resources/images/member/box.png">
					        </td>
			         	</tr>
			         	<tr><td><br><br></td></tr>
			         	<tr>
			         		<td align="center">
			         			&nbsp;<button class="goShoppingBtn" type="button" onclick="location.href='${pageContext.servletContext.contextPath}/product/productList?prodCtgNo=PRCT0001'">상품 보러가기</button>
			         		</td>
			         	</tr>
			         	
			         </table>
               
               </c:if>     
         
         
                    
		
		<div class="pagingArea" align="center">
			<button id="startPage">◀◀</button>
	
			<c:if test="${ pageInfo.pageNo == 1 }">
				<button disabled>◁</button>
			</c:if>
			<c:if test="${ pageInfo.pageNo > 1 }">
				<button id="prevPage">◁</button>
			</c:if>
	
			<c:forEach var="p" begin="${ pageInfo.startPage }" end="${ pageInfo.endPage }" step="1">
			<c:if test="${ pageInfo.pageNo eq p }">
				<button disabled><c:out value="${ p }"/></button>
			</c:if>
			<c:if test="${ pageInfo.pageNo ne p }">
				<button onclick="pageButtonAction(this.innerText);"><c:out value="${ p }"/></button>
			</c:if>
			</c:forEach>
	
			<c:if test="${ pageInfo.pageNo == pageInfo.maxPage }">
				<button disabled>▷</button>	
			</c:if>
			<c:if test="${ pageInfo.pageNo < pageInfo.maxPage }">
				<button id="nextPage">▷</button>
			</c:if>
	
			<button id="maxPage">▶▶</button>
			
			
			<c:if test="${!empty searchValue}"></c:if>
			
					<form action="${pageContext.servletContext.contextPath}/member/search/purchaseList" method="Get">
                    	<div>
                    		
                    			<select id="SearchCondition" name="condition">
	 
									<option value="purchasedDate" <c:if test="${condition eq 'purchasedDate' }">selected</c:if> >구매일시</option>
									<option value="purchaseNo" <c:if test="${condition eq 'purchaseNo' }">selected</c:if> >구매번호</option>
									<option value="userName"  <c:if test="${condition eq 'userName' }">selected</c:if> >주문자</option>
									<option value="prodName"  <c:if test="${condition eq 'prodName' }">selected</c:if> >주문상품명</option>
									<option value="orderPrice"   <c:if test="${condition eq 'orderPrice' }">selected</c:if> >주문금액</option>
							
								</select> 
							
							<input type="hidden" name="userNo" value="${loginMember.userNo}">
							<input type="search" id="SearchValue" name="value" value="${searchValue}">
							<button type="submit" class="btn">검색</button>
           	        	</div>	
           			</form>
           	
           	
           	
<%--            	<c:if test="${empty searchValue}">
           	
           			
					<form action="${pageContext.servletContext.contextPath}/member/search/purchaseList" method="Get">
                    	<div>
                    		
                    			<select id="SearchCondition" name="condition">
	 
									<option value="purchasedDate">구매일시</option>
									<option value="purchaseNo">구매번호</option>
									<option value="userName">주문자</option>
									<option value="prodName">주문상품명</option>
									<option value="orderPrice">주문금액</option>
							
								</select> 
							
							<input type="hidden" name="userNo" value="${loginMember.userNo}">
							<input type="search" id="SearchValue" name="value">
							<button type="submit" class="btn">검색</button>
           	        	</div>	
           			</form>
           			
           			
           	</c:if>	 --%>	
           			
		</div>

        
        
        
        </section>
        </div>
    </div>
</div>	
	
	

     <div class="footer">
    
    <jsp:include page="../common/footer.jsp"/>
    
	</div> 
	
	
	<script>
		const link = "${ pageContext.servletContext.contextPath }/member/purchaseList";
		
		function pageButtonAction(text) {
			location.href = link + "?currentPage=" + text + "&userNo=${loginMember.userNo}";
		}
		
		if(document.getElementById("startPage")){
			const $startPage = document.getElementById("startPage");
			$startPage.onclick = function(){
				location.href = link + "?currentPage=1" + "&userNo=${loginMember.userNo}";
			}
		}
		
		if(document.getElementById("maxPage")){
			const $maxPage = document.getElementById("maxPage");
			$maxPage.onclick = function(){
				location.href = link + "?currentPage=${ pageInfo.maxPage }" + "&userNo=${loginMember.userNo}";
			}
		}
		
		if(document.getElementById("prevPage")){
			const $prevPage = document.getElementById("prevPage");
			$prevPage.onclick = function(){
				location.href = link + "?currentPage=${ pageInfo.pageNo - 1 }" + "&userNo=${loginMember.userNo}";
			}
		}
		
		if(document.getElementById("nextPage")){
			const $nextPage = document.getElementById("nextPage");
			$nextPage.onclick = function(){
				location.href = link + "?currentPage=${ pageInfo.pageNo + 1 }" + "&userNo=${loginMember.userNo}";
			}
		}
	
	
	</script>
	
	
	
	
	
	
</body>
</html>