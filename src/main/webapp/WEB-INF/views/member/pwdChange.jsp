<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>==지금, 보고 싶다 == 비밀번호 변경</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="/missingitnow/resources/css/member/pwdChange.css"> 

<style>
	#changeNotice{
	
		font-size:13px;
		color:red;
		line-height:29px;
	}

</style>


</head>


    <script>
    
    
    /* 비밀번호 변경 실패시 나오는 경고창 */
    
       $(document).ready(function(){
    	
    	
	
    		
    	if ("${message}"===""){
    		
    		    		
    	} else {
    		         
    		
    		alert("${message}");
    		
    	}
    	
    	
    	
    });
     
    </script>

<body>

	
	<div class="header">
	<jsp:include page="../common/header.jsp"/>
    </div>


<div>
<img class="leftBg" src="/missingitnow/resources/images/member/findLeftbg.png">
</div> 
	

    <div><br></div>
    <div class="pwdChangeForm">
    <form id="pwdForm" action="${pageContext.servletContext.contextPath}/member/pwdChange" method="post" onsubmit="return checkAll();">
     <div><br><br></div>  	
       	<table class="titleTable" border="1">
		   	<tr>
		   		<td class="titleColorBox"><br><br><br><c:out value="${loginMember.userName}"/> 회원님의 </td>
		   		<td class="titleNonColor">&nbsp;&nbsp;비밀번호를 변경합니다.</td>
		   		
		   	</tr>
 
		   		<tr><td><br></td></tr>

	</table> 
       	
       	
       	
       	<div class="pwdChangeTitle">            
            <input type="hidden" name="userId" value="${loginMember.userId }"/>  
             
            <br><br>            
        </div>
	
	<div class="whiteForm">
		<div><br><br><br></div>
        <table class="pwdChangeFormTable">
            <tr>
                <td class="label">
                	&nbsp;&nbsp;현재 비밀번호 : </td>
                	<td>&nbsp;&nbsp; <input class="inputPassword" type="password" name="currentPassword" id="currentPassword">
                </td>
            </tr>
            
            <tr><td><br></td></tr>
            
            <tr><td colspan="2" id="changeNotice">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;비밀번호는 8~16자, 숫자와 영문자 조합 (특수문자 허용 X)</td></tr>
            <tr><td><br></td></tr>
            <tr>
                <td class="label">&nbsp;&nbsp;새 비밀번호 : </td>
                	<td>&nbsp;&nbsp;  <input class="inputPassword" type="password" name="newPassword" id="newPassword" required>
                </td>
            </tr>
            <tr><td><br></td></tr>
            
            
            
            <tr>
                <td class="label">
                	&nbsp;&nbsp;새 비밀번호 재입력 
                	</td> <td>&nbsp;&nbsp; <input class="inputPassword" type="password" name="newPasswordRetype" id="newPasswordRetype" required>
                </td>
            </tr>
           
            <tr><td colspan="2">
            	
            	<p id="pwdCheck"></p>
            
            </td></tr>
           
           <tr><td><br><br></td></tr>
           
            <tr align="center">
                <td colspan="4"> &nbsp;&nbsp;<button id="pwdChangeBtn" class="mainBtn" type="submit">비밀번호 변경</button> &nbsp;&nbsp;   
                     <button class="cancelBtn" type="button">취소</button>  </td>

            </tr>
        </table>
        <div><br><br></div>
	</div>
    </form>
   </div>
	
	
	
	<script>
	
	
	const pwdCheck = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,16}$/;  
	   
	
	
			 function checkAll(){
				
				const val = $('#newPasswordRetype').val();
				
				
				console.log(val);
				
					if(val==''){
						
						console.log("비밀번호를 입력해주세요");
						
						alert("공란이 확인되었습니다. 비밀번호를 입력해주세요.");
						
						return false;
						
					}
					 
			 
			 }				 
					 
			 	$(function(){
					
			 		
					$("#newPassword").on("propertychange change keyup paste input", function(){
						  
						const newPassword = $("#newPassword").val();
						const newPasswordRetype = $("#newPasswordRetype").val();
							   
						 
						 if(!(pwdCheck.test(newPassword))){
							   
							   $("#pwdCheck").text("비밀번호는 8~16자, 숫자와 영문자 조합 (특수문자 허용 X)")
					           $('#pwdCheck').css('color', 'red');
							   $('#pwdChangeBtn').css('background-color', 'black');
							  
					    	   
							   
						   } else if((pwdCheck.test(newPassword))){
							   
							   $("#pwdCheck").text('');   
						 
							   
							   
					    		   
					    		   
					    		   if(newPassword!=newPasswordRetype){
										 
									   $("#pwdCheck").text("일치하지 않습니다.")
							           $('#pwdCheck').css('color', 'red');				   
							    	   $('#pwdChangeBtn').css('background-color', 'black');   
							    	   $("#pwdForm").attr("onsubmit", "return false"); 
									 
								 } else if(newPassword==newPasswordRetype) {
									 
									 $("#pwdCheck").text('');				   
							    	   $('#pwdChangeBtn').css('background-color', '#775EEE'); 
							    	   
													    	   
								 }
					    		   
					    		
							   				   
						   }
						 
						  
						 
					});	
					
					
					$("#newPasswordRetype").on("propertychange change keyup paste input", function(){
					   		   		
						const newPassword = $("#newPassword").val();
						const newPasswordRetype = $("#newPasswordRetype").val();
						   
						
							if(!(pwdCheck.test(newPasswordRetype))){
								   
								   $("#pwdCheck").text("비밀번호는 8~16자, 숫자와 영문자 조합 (특수문자 허용 X)")
						           $('#pwdCheck').css('color', 'red');				   
						    	   $('#pwdChangeBtn').css('background-color', 'black');   
						    	   $("#pwdForm").attr("onsubmit", "return false"); 
						    	   
								   
							   } else if((pwdCheck.test(newPasswordRetype))){
								   
								   $("#pwdCheck").text('');				   
						    	   $('#pwdChangeBtn').css('background-color', '#775EEE');  
						    	   $("#pwdForm").attr("onsubmit", "return true"); 
						    	   
						    	     
						    		   if(newPassword!=newPasswordRetype){
											 
											 $("#pwdCheck").text("일치하지 않습니다.")
									           $('#pwdCheck').css('color', 'red');				   
									    	   $('#pwdChangeBtn').css('background-color', 'black');   
									    	   $("#pwdForm").attr("onsubmit", "return false"); 
											 
										 } else if(newPassword==newPasswordRetype) {
											 
											 $("#pwdCheck").text('');				   
									    	 $('#pwdChangeBtn').css('background-color', '#775EEE'); 
									    	 $("#pwdForm").attr("onsubmit", "return true");
									    	   
									    	 								 
										 }
						    		   
						    		   
						    	   
						    	   }
						    	   
						    	 
					   
					});	
					
					
				});	
 
	</script>
	
	
	
	
	<div class="footer">
    
    <jsp:include page="../common/footer.jsp"/>
    
	</div>	
		
</body>
</html>