<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="/missingitnow/resources/js/member/myPageNav.js"></script>
<link rel="stylesheet" href="/missingitnow/resources/css/member/cancelExchangeRefund.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

<style>

	#tab-1{		
		width:700px;		
	}
	
	#tab-2{
	background-color:;
		width:800px;
	}

	#exchangePagingArea{
		margin-top:325px;
		margin-left:100px;
	}
	
	#refundPagingArea{
		margin-top:-100px;
	
	}
	
	#exchangeCondition, #searchInput, #refundSearchValue, #refundSearchCondition{
	
		border:1px solid black;
	}
	
	#searchBtn, #refundSearchBtn{
	
		color:#ffff;
		background:rgba(119, 94, 238);
		width:40px;
		height:20px;
		border-radius: 10px;
		font-size:13px;
	}
	
		
</style>

</head>
<body>
			
 	<div class="header">
	<jsp:include page="../common/header.jsp"/>
	
	</div>  
<div>
<img class="leftBg" src="/missingitnow/resources/images/member/purchaseListLeft.png">
</div> 	
<div id="contentForm">	
    <div id="registFormTitle">
   
   
   		<div></div>
	   <table class="titleTable">
	   
	   		
		   	<tr>
		   		<td class="titleColorBox"><br><br><br><br><c:out value="${loginMember.userName }" />&nbsp;회원님의&nbsp; </td> 
		   		<td class="titleNonColor">교환반품내역입니다.</td>
		   	</tr>
	   </table>           
   
     	
   </div>

    <div id="totalForm">
        
             	<form name="Paging">
                     <input type="hidden" name="userNo" value="${loginMember.userNo}">				
				</form>
    	
    
    		<div class="leftDiv">
    		
    		
                <table class="myPageNavTable">
                    <tr>
                    <td align="center"><br>  <i id="userImg" class="fas fa-user"></i> <!--프로필 사진 영역-->
                    <br></td>
                    </tr>
                    
                    <tr>
                        <td class="welcomeTd" align="center">
                            <c:out value="${ loginMember.userName }"/>님 안녕하세요!
                        </td>
                    </tr>
					<tr> <td><br></td> </tr>
                    <tr>
                        <td>
                        <a href="${pageContext.servletContext.contextPath}/member/myPageMain">
                        		&nbsp;프로필
                        		</a>
                        		</td>
                    </tr>
                    <tr> <td><br></td> </tr>
                    <tr>
                        <td>
                        <a href="javascript:goPurchaseListPage('${loginMember.userNo}');">
                        &nbsp;구매내역
                        </a>
                        </td>
                    </tr>
                    <tr> <td><br></td> </tr>
                    <tr>
						 <td class="pointTd">
						<a href="javascript:goCancelExchangeRefundPage('${loginMember.userNo}');">
                        &nbsp;취소/교환/반품내역
                        </a>
                        </td>
                    </tr>
                    <tr> <td><br></td> </tr>

                    <tr>
                        <td>
                        <a href="javascript:goQnaPage('${loginMember.userNo}');">
                        &nbsp;Q&A
                        </a>
                        </td>
                    </tr>

                    <tr> <td><br></td> </tr>
<!--                     <tr>
                        <td>&nbsp;쿠폰</td>
                    </tr>
                    <tr> <td><br></td> </tr> -->
                    <tr>
                        <td>
                        <a href="javascript:goReviewPage('${loginMember.userNo}');">
                        &nbsp;리뷰내역
                        </a>
                        </td>
                    </tr>
                    <tr> <td><br></td> </tr>
                </table>

            
        </div>
        
        <div class="centerBlank"><br></div>
        
        
        <div id="colorBox">
    
        </div>
		
	
		
		
        <div id="rightDiv">

            <section>
                    <script>
                    

                    </script>
                    
                    
                        <div class="container">

					        <ul class="tabs">
					            <li class="tab-link current" data-tab="tab-1">교환 내역</li>
					            <li class="tab-link" data-tab="tab-2">반품 내역</li>
					            
					        </ul>
					    
					        <div id="tab-1" class="tab-content current">
					    
					    
					    
					    					<c:if test="${!empty exchangeList }">
                <table class="purchaseListTable" border="1">
					<tr><td><br></td></tr>
                    <tr class="bottomLineTr">                        
                        <th>교환번호</th>
                        <th>주문하신 분</th>
                        <th>교환일자</th>
                        <th>주문상품명</th>
                        <th>주문금액</th>
                    </tr>
                    <c:forEach items="${exchangeList}" var="exchange">
                    	<tr class="bottomLineTr" align="center">                    	
	                    	<td><c:out value="${exchange.exchangeNo}"/></td>
	                    	<td><c:out value="${exchange.privateMemberDTO.userName}"/></td>            							
	                    	<td><c:out value="${exchange.exchangeDate}"/></td>
	                    	<td><c:out value="${exchange.productDTO.prodName}"/></td>
	                    	<td><c:out value="${exchange.orderDTO.orderPrice}"/></td>
                    	</tr>        	
                    </c:forEach>  
					
                 	
                    
                                     
			     </table>
                    
               </c:if> 
                    
               <c:if test="${empty exchangeList}"> 
               		
               		<table class="emptyTable">
			         	<tr><td><br><br></td></tr>
			         	<tr>
			         		<td class="emptyMessage" align="center">&nbsp;아직 구매하신 내역이 없습니다.</td>
			         	</tr>
			         	
			         	<tr><td><br><br><br></td></tr>
			         	
			         	<tr>
			         		<td>
			        			<img class="emptyBox" src="/missingitnow/resources/images/member/box.png">
					        </td>
			         	</tr>
			         	<tr><td><br><br></td></tr>

			         	
			         </table>
               
               </c:if>    
                    <br><br><br><br><br><br><br>
				<div id="exchangePagingArea" class="pagingArea" align="center">
			<button id="startPage">◀◀</button>
	
			<c:if test="${ exchangePageInfo.pageNo == 1 }">
				<button disabled>◁</button>
			</c:if>
			<c:if test="${ exchangePageInfo.pageNo > 1 }">
				<button id="prevPage">◁</button>
			</c:if>
	
			<c:forEach var="p" begin="${ exchangePageInfo.startPage }" end="${ exchangePageInfo.endPage }" step="1">
			<c:if test="${ exchangePageInfo.pageNo eq p }">
				<button disabled><c:out value="${ p }"/></button>
			</c:if>
			<c:if test="${ exchangePageInfo.pageNo ne p }">
				<button onclick="pageButtonAction(this.innerText);"><c:out value="${ p }"/></button>
			</c:if>
			</c:forEach>
	
			<c:if test="${ exchangePageInfo.pageNo == exchangePageInfo.maxPage }">
				<button disabled>▷</button>	
			</c:if>
			<c:if test="${ exchangePageInfo.pageNo < exchangePageInfo.maxPage }">
				<button id="nextPage">▷</button>
			</c:if>
	
			<button id="maxPage">▶▶</button>
			
			<form action="${pageContext.servletContext.contextPath}/member/search/cancelExchangeList" method="Get">
	                    	<div>
	                    	<br>	
	                    			<select id="exchangeCondition" name="exchangeCondition">
		 
										<option value="userName">주문하신 분</option>
										<option value="exchangeDate">교환일자</option>
										<option value="prodName">주문상품명</option>																
										<option value="orderPrice">주문금액</option>																
								
									</select> 
								
								<input type="hidden" name="userNo" value="${loginMember.userNo}">
								<input type="search"  id="searchInput" name="exchangeValue" value="">
								<button type="submit" id="searchBtn" class="btn">검색</button>
	           	        	</div>	
	           	</form>
			
			
			
			
			
		</div>
					    
			
					    
					    
					        </div>
					        
					        
					        
					        
					        
					        
					        
					        
					        
					        
					        <div id="tab-2" class="tab-content">
					  
				<c:if test="${!empty refundList }">	  
					<table id="refundTable" class="purchaseListTable" border="1">
					
						<tr><td><br></td></tr>
	                	<thead>
	                    <tr class="bottomLineTr">                        
	                        <th>반품번호</th>
	                        <th>주문하신 분</th>
	                        <th>반품일자</th>
	                        <th>주문상품명</th>
	                        <th>주문금액</th>
	                    </tr> 
						</thead>					  
						 <c:forEach items="${refundList}" var="refund">
						<tbody>	
	                    	<tr class="bottomLineTr" align="center">
		                     	<td id="returnNo"><c:out value="${refund.returnNo}"/></td>            							
		                    	<td id="userName"><c:out value="${refund.userName}"/></td>            							
		                    	<td id="returnDate"><c:out value="${refund.returnDate}"/></td>             
		                    	<td id="prodName"><c:out value="${refund.prodName}"/></td>
		                    	<td id="orderPrice"><c:out value="${refund.orderPrice}"/></td>
	                    	
	                    	</tr>
	                    </tbody>	
						</c:forEach>
					
					</table>	
				</c:if>		  
				
				
				<c:if test="${empty refundList}"> 
               		
               		<table class="emptyTable">
			         	<tr><td><br><br></td></tr>
			         	<tr>
			         		<td class="emptyMessage" align="center">&nbsp;아직 구매하신 내역이 없습니다.</td>
			         	</tr>
			         	
			         	<tr><td><br><br><br></td></tr>
			         	
			         	<tr>
			         		<td>
			        			<img class="emptyBox" src="/missingitnow/resources/images/member/box.png">
					        </td>
			         	</tr>
			         	<tr><td><br><br></td></tr>

			         	
			         </table>
               
               </c:if>  
				                    <br><br><br><br><br><br><br>
				<div id="refundPagingArea" class="pagingArea" align="center">
			<button id="startPage">◀◀</button>
	
			<c:if test="${ refundPageInfo.pageNo == 1 }">
				<button disabled>◁</button>
			</c:if>
			<c:if test="${ refundPageInfo.pageNo > 1 }">
				<button id="prevPage">◁</button>
			</c:if>
	
			<c:forEach var="p" begin="${ refundPageInfo.startPage }" end="${ refundPageInfo.endPage }" step="1">
			<c:if test="${ refundPageInfo.pageNo eq p }">
				<button disabled><c:out value="${ p }"/></button>
			</c:if>
			<c:if test="${ refundPageInfo.pageNo ne p }">
				<button onclick="pageButtonAction(this.innerText);"><c:out value="${ p }"/></button>
			</c:if>
			</c:forEach>
	
			<c:if test="${ refundPageInfo.pageNo == refundPageInfo.maxPage }">
				<button disabled>▷</button>	
			</c:if>
			<c:if test="${ refundPageInfo.pageNo < refundPageInfo.maxPage }">
				<button id="nextPage">▷</button>
			</c:if>
	
			<button id="maxPage">▶▶</button>
			
			<form action="${pageContext.servletContext.contextPath}/member/search/refundList" method="Get">
	                    	<div>
	        					<br>            		
	                    			<select id="refundSearchCondition" name="refundSearchCondition">
		 
										<option value="userName">주문하신 분</option>
										<option value="returnDate">반품일자</option>										
										<option value="prodName">주문상품명</option>																
										<option value="orderPrice">주문금액</option>																
								
									</select> 
								
								<input type="hidden" name="userNo" value="${loginMember.userNo}">
								<input type="search" id="refundSearchValue" name="refundValue" value="">
								<button type="button" id="refundSearchBtn" class="btn">검색</button>
	           	        	</div>	
	           	</form>
			
			
			
		</div>
				
				
				
				
				
				
					        </div>
					  
					    
					    </div>
                    
                    
                    
          <script>
			
			$(function(){
				
				
				$("#refundSearchBtn").click(function(){
					
					console.log("refundSearchBtn Click");
					
					var refundSearchCondition = $("#refundSearchCondition").val();
					var refundSearchValue = $("#refundSearchValue").val();
					var userNo = "${loginMember.userNo}";
					
					if(refundSearchValue == ""){
						
						alert("공백");
						return false;
						
					} else if (refundSearchValue != ""){
						
						console.log(refundSearchCondition);
						console.log(refundSearchValue);
							
						var arr = new Array();
						
						arr.push(refundSearchCondition);
						arr.push(refundSearchValue);
						arr.push(userNo);
						console.log("push after arr : " + arr);
						
						
						
						
						
						 $.ajax({
							
							
							type:"GET",
							url : "${pageContext.servletContext.contextPath}/member/search/refundList",
							data: {
								
								arr : arr
								
								},
							
								
							success : function(data){
								
								console.log(data);
								
								 $("#refundTable > tbody").empty(); 
								
								for(var i=0 ; i < data.length ; i++){
									
									$("#refundTable").append("<tr><td>"+data[i].returnNo+"</td><td>"+data[i].userName+"</td><td>"+data[i].returnDate+"</td><td>"+data[i].prodName+"</td><td>"+data[i].orderPrice+"</td></tr>")
									
									
								}
								
																
								
							}
								
							
							
						});
						 
						
						
					}
					
					
					
					
				});
				
				
			});
			
				
			
			
			</script>          
     
        </section>
        </div>
    </div>
</div>	
	
	

     <div class="footer">
    
    <jsp:include page="../common/footer.jsp"/>
    
	</div> 
	
	
	
		<script>
		
		
	
	    $(document).ready(function(){
			
			$('ul.tabs li').click(function(){
				var tab_id = $(this).attr('data-tab');
	
				$('ul.tabs li').removeClass('current');
				$('.tab-content').removeClass('current');
	
				$(this).addClass('current');
				$("#"+tab_id).addClass('current');
				
			});
			
		
	});
		
		
		
		
		
		console.log(${pageInfo.maxPage});
		
		const link = "${ pageContext.servletContext.contextPath }/member/cancelExchangeRefund";
		
		function pageButtonAction(text) {
			location.href = link + "?currentPage=" + text + "&userNo=${loginMember.userNo}";
		}
		
		if(document.getElementById("startPage")){
			const $startPage = document.getElementById("startPage");
			$startPage.onclick = function(){
				location.href = link + "?currentPage=1" + "&userNo=${loginMember.userNo}";
			}
		}
		
		if(document.getElementById("maxPage")){
			const $maxPage = document.getElementById("maxPage");
			$maxPage.onclick = function(){
				location.href = link + "?currentPage=${ pageInfo.maxPage }" + "&userNo=${loginMember.userNo}";
			}
		}
		
		if(document.getElementById("prevPage")){
			const $prevPage = document.getElementById("prevPage");
			$prevPage.onclick = function(){
				location.href = link + "?currentPage=${ pageInfo.pageNo - 1 }" + "&userNo=${loginMember.userNo}";
			}
		}
		
		if(document.getElementById("nextPage")){
			const $nextPage = document.getElementById("nextPage");
			$nextPage.onclick = function(){
				location.href = link + "?currentPage=${ pageInfo.pageNo + 1 }" + "&userNo=${loginMember.userNo}";
			}
		}
	
	</script>
		
	

		
</body>
</html>